# node-chrome

## What ?

A simple image, based on [Debian Docker official image](https://hub.docker.com/_/debian), with some additions :
- Node.js from [nodesource](https://github.com/nodesource/distributions)
- Latest Google Chrome from [official package repository](http://dl.google.com/linux/chrome/deb/)

Version number is related to Node.js major version, for example `node-chrome:16.0.0` contains latest `nodejs 16` version.
## Why ?

To run Angular CI from Gitlab or any other app that needs Node.js and Google Chrome binaries.

## How ?
### Distribution

Pull image from [Docker Hub](https://hub.docker.com/repository/docker/tibidibtibo/node-chrome)

### Sources

Fork or watch sources from [Gitlab repository](https://gitlab.com/tibidibtibo/node-chrome).
