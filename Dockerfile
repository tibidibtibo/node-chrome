FROM node:20-alpine3.20

# Prepare tools
RUN apk update
RUN apk add --no-cache \
    chromium \
    nss \
    freetype \
    harfbuzz \
    ttf-freefont \
    git \
    bash

ENV CHROME_BIN=/usr/bin/chromium-browser \
    CHROME_PATH=/usr/lib/chromium/

# Install Cypress dependencies
RUN npm install -g cypress

# User non-root
RUN addgroup -S chrome && adduser -S -G chrome chrome
USER chrome

# Run Chrome non-privileged
CMD ["chromium-browser", "--no-sandbox", "--headless", "--disable-gpu", "--remote-debugging-address=0.0.0.0", "--remote-debugging-port=9222"]
